package gameBoard;

public class Controller {
	private Model model;
	private View view;
	private int simulationDelay = 2000; //can be used to make the game harder or easier!
	
	public Controller() {
		//create a new model and view
		newModelAndView();
		//run the simulation
		Simulate();
	}
	
	public void newModelAndView() {
		model = new Model();
		view = new View();
	}
	
	
	public void Simulate() {
		while (!model.gameOver()) {
			model.addBlock(new Block());  //add a new block to the game board
			while (model.getBlock().getFalling()) {
				model.moveBlock();		  //keep moving block while it can move
			}
			model.addBlockToModel();
			model.stabilize();		//removes full rows and cascades all rows/units until stable
		}
		
	}
	
}
