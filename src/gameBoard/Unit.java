package gameBoard;

public class Unit {
	private int row;
	private int col;
	
	public Unit(int row, int col) {
		this.row = row;
		this.col = col;
	}
	
	public int getRow() {
		return row;
	}
	
	public int getCol() {
		return col;
	}
	
	public void moveRight() {
		col++;
	}
	
	public void moveLeft() {
		col--;
	}

	public void moveDown() {
		row--;
	}
	
}
