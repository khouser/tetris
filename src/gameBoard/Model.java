package gameBoard;



public class Model {
	public static final int HEIGHT = 20;
	public static final int WIDTH = 10;
	private int[][] grid;
	private int[][] fallingblock;
	private Block block;
	
	
	public Model() {
		grid = new int[HEIGHT][WIDTH]; //need to initialize all to -1 (if full, will be 0-6)
		for (int i = 0; i < HEIGHT; i++) {
			for (int j = 0; j < WIDTH; j++) {
				grid[i][j] = -1;
				fallingblock[i][j] = -1;
			}
		}
	}
	
	public void addBlock(Block block) {
		this.block = block;
		for (Unit u: block.getUnits()) {
			fallingblock[u.getRow()][u.getCol()] = block.getType();
		}
	}
	
	public void addBlockToModel() {
		for (Unit u : block.getUnits()) {
			grid[u.getRow()][u.getCol()] = block.getType();
		}
		for (int i = 0; i < HEIGHT; i++) {
			for (int j = 0; j < WIDTH; j++) {
				fallingblock[i][j] = -1;
			}
		}
	}
	
	public Block getBlock() {
		return block;
	}
	
	//progresses falling block downward
	public void moveBlock() {
		//checks to make sure it can fall there
		boolean flag = true;
		for (Unit u : block.getUnits()) {
			if (grid[u.getRow()-1][u.getCol()] != -1) {
				flag = false;
				block.setFalling(false);
			}
		}
		if (flag) {  
			//reconfigure grid for new block coordinates
			for (Unit u: block.getUnits()) {
				fallingblock[u.getRow()][u.getCol()] = -1;
				fallingblock[u.getRow()-1][u.getCol()] = block.getType();
			}
			block.moveDown();
		}
		
	}
	
	//checks if any rows are full
	private boolean areThereFullRows() {
		boolean flag = false;
		for (int i = 0; i < HEIGHT; i++) {
			if (checkFullRow(i) == true) {
				flag = true;
			}
		}
		return flag; //true if there are any full rows in the grid
	}
	//checks if row is full 
	private boolean checkFullRow(int i) {
		for (int j = 0; j < WIDTH; j++) {
			if (grid[i][j] == -1) {
				return false;
			}
		}
		return true;
	}
	
	//checks if row is empty 
	private boolean checkEmptyRow(int i) {
		for (int j = 0; j < WIDTH; j++) {
			if (grid[i][j] != -1) {
				return false;
			}
		}
		return true;
	}
	
	private void setEmptyRow(int i) {
		for (int j = 0; j < WIDTH; j++) {
			grid[i][j] = -1;
		}
	}

	//deletes all full rows
	public void removeFullRows() {
		for (int i = 0; i < HEIGHT; i++) {
			if (checkFullRow(i)) {
				for (int j = 0; j < WIDTH; j++) {
					grid[i][j] = -1;
				}
			}
		}
	}
	
	private void cascadeRows() {
		for (int i = 0; i < HEIGHT; i++) {
			if (checkEmptyRow(i)) {
				for (int c = i + 1; c < HEIGHT; c++) {
					if (!checkEmptyRow(c)) {
						for (int j = 0; j < WIDTH; j++) {
							grid[i][j] = grid[c][j];
						}
						setEmptyRow(c);
					}
				}
			}
		}
	}
	
	//moves downwards
	private void cascadeUnits() {
		for (int i = HEIGHT - 1; i > 0; i--) {
			for (int j = 0; j < WIDTH; j++) {
				if (grid[i][j] != -1)  {	//makes sure there is a unit there
					if (grid[i-1][j] == -1)	{//makes sure the space bellow is empty
						grid[i-1][j] = grid[i][j];
						grid[i][j] = -1;
					}
				}
			}
		}
	}
	
	//Checks for full rows
	//Deletes full rows
	//Cascades all rows so all empty rows are at the top and non-empty ones are in order on bottom
	//Cascades units if there is nothing bellow the unit, then repeats process until no more full rows
	
	public void stabilize() {
		while (areThereFullRows()) {
			removeFullRows();
			cascadeRows();
			cascadeUnits();
		}
		
	}
	//checks if there is nowhere else for a block to go in the grid
	public boolean gameOver() {
		return false;
	}
	


}
