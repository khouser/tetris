 package gameBoard;

import java.util.ArrayList;


public class Block {
	private int type;
	private boolean falling;
	private ArrayList<Unit> units = new ArrayList<Unit>();
	private int r0, r1, r2, r3;
	private int c0, c1, c2, c3;
	
	public Block() {
		setType();
		setFalling(true);
		setRowsandCols();
		setUnits();
	}
	
	//returns random number between 0-6 (7 types of blocks)
	private void setType() {
		type = (int)(Math.random() * 7);
	}
	//public because if the block is moved the model can set it
	public void setFalling(boolean falling) {
		this.falling = falling;
	}
	public int getType() {
		return type;
	}
	
	public boolean getFalling() {
		return falling;
	}
	
	private void setUnits() {
		units.set(0, new Unit(r0, c0));
		units.set(1, new Unit(r1, c1));
		units.set(2, new Unit(r2, c2));
		units.set(3, new Unit(r3, c3));
	}
	
	public ArrayList<Unit> getUnits() {
		return new ArrayList<Unit>(units);
	}

	private void setRowsandCols() {
		switch (type) {
			case 0:
				r0 = 20; r1 = 20; r2 = 20; r3 = 20;
				c0 = 3; c1 = 4; c2 = 5; c3 = 6;
				break;
			case 1:
				r0 = 20; r1 = 20; r2 = 19; r3 = 19;
				c0 = 4; c1 = 5; c2 = 4; c3 = 5;
				break;
			case 2:
				r0 = 20; r1 = 19; r2 = 19; r3 = 19;
				c0 = 4; c1 = 4; c2 = 5; c3 = 6;
				break;
			case 3:
				r0 = 19; r1 = 19; r2 = 19; r3 = 20;
				c0 = 4; c0 = 5; c0 = 6; c0 = 7;
				break;
			case 4:
				r0 = 19; r1 = 19; r2 = 20; r3 = 20;
				c0 = 4; c1 = 5; c2 = 5; c3 = 6;
				break;
			case 5: 
				r0 = 20; r1 = 20; r2 = 19; r3 = 19;
				c0 = 4; c1 = 5; c2 = 5; c3 = 6;
				break;
			case 6:
				r0 = 20; r1 = 19; r2 = 19; r3 = 19;
				c0 = 5; c1 = 4; c2 = 5; c3 = 6;
				break;
			default:
				//throw error
		}
		
	}
	

	public void moveDown() {
		//Checks if it going outside bounds -- not if it will run into something else :P
		boolean flag = true;
		for (Unit u: units) {
			if (u.getRow() == 0) {
				flag = false;
			}
		}
		if (flag) {
			for (Unit u: units) {
				u.moveDown();
			}
		}
	}

	public void moveRight() {
		boolean flag = true;
		for (Unit u: units) {
			if (u.getCol() == 9) {
				flag = false;
			}
		}
		if (flag) {
			for (Unit u: units) {
				u.moveRight();
			}
		}
		
	}

	public void moveLeft() {
		boolean flag = true;
		for (Unit u: units) {
			if (u.getCol() == 0) {
				flag = false;
			}
		}
		if (flag) {
			for (Unit u: units) {
				u.moveLeft();
			}
		}
	}
}

